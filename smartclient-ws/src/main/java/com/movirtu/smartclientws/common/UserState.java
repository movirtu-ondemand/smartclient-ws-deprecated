package com.movirtu.smartclientws.common;

public enum UserState {

	UNREGISTERED(0),
	AWAIT_CODE(1),
	CODE_VERIFIRED(2),
	AUTHENTICATED(3);

	int state=-1;

	UserState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public static UserState getUserState(int value) {
		try {
			return UserState.values()[value];
		}catch( ArrayIndexOutOfBoundsException e ) {
			throw new IllegalArgumentException("Unknown enum value :"+ value);
		}
	}

}
