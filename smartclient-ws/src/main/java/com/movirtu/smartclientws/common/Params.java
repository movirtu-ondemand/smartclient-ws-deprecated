/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.common;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.Criteria;
/**
 *
 * @author david
 */
@XmlRootElement(name = "params")
public class Params {
	String api_token;
	ProfileDetails profileDetails;
	SubscriberDetails subscriberDetails;

	public ProfileDetails getProfileDetails() {
		return profileDetails;
	}

	public void setProfileDetails(ProfileDetails profileDetails) {
		this.profileDetails = profileDetails;
	}

	public String getApi_token() {
		return api_token;
	}

	public void setApi_token(String api_token) {
		this.api_token = api_token;
	}

	public ProfileDetails getUserDetails() {
		return profileDetails;
	}

	public void setUserDetails(ProfileDetails userDetails) {
		this.profileDetails = userDetails;
	}

	public SubscriberDetails getSubscriberDetails() {
		return subscriberDetails;
	}

	public void setSubscriberDetails(SubscriberDetails subscriberDetails) {
		this.subscriberDetails = subscriberDetails;
	}

}
