package com.movirtu.smartclientws.common;

import java.util.Map;

import org.apache.log4j.Logger;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;


public class HttpURLConnectionManager {

	private static Logger logger = Logger.getLogger(HttpURLConnectionManager.class);

	/**
	 * This method sends Http post requests
	 * @throws Exception
	 */
	public static String sendHttpPost(Params param, Map<String,Object> postBody) throws RuntimeException {
		String output=null;
		try{
			ClientConfig clientConfig = new DefaultClientConfig();              
			clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);     
			Client client = Client.create(clientConfig);

			param.setApi_token("movirtuInternal");
			postBody.put("id","12345");
			postBody.put("jsonrpc","2.0");

			logger.debug("Request Param Map .... \n"+postBody);
			WebResource webResource = client.
					resource("http://54.65.233.22/WServiceWFE/manyme/request/mclient/");

			ClientResponse response = webResource.accept("application/json").
					type("application/json").post(ClientResponse.class, postBody);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "+ response.getStatus());
			}
			output = response.getEntity(String.class);
			logger.debug("Output from Server .... \n");
			logger.debug(output);
		}catch(Exception ex){
			logger.debug("Exception in http request ",ex);
		}
		return output;
	}

	//	public String sendHttpPost(String url, StringEntity jsonEntity) throws Exception {
	//		logger.debug("sendGet http get url=" + url);
	//		try {
	//			 
	//			DefaultHttpClient httpClient = new DefaultHttpClient();
	//			HttpPost postRequest = new HttpPost(
	//				"http://localhost:8080/RESTfulExample/json/product/post");
	//	 
	//			StringEntity input = new StringEntity("{\"qty\":100,\"name\":\"iPad 4\"}");
	//			input.setContentType("application/json");
	//			postRequest.setEntity(input);
	//	 
	//			HttpResponse response = httpClient.execute(postRequest);
	//	 
	//			if (response.getStatusLine().getStatusCode() != 201) {
	//				throw new RuntimeException("Failed : HTTP error code : "
	//					+ response.getStatusLine().getStatusCode());
	//			}
	//	 
	//			BufferedReader br = new BufferedReader(
	//	                        new InputStreamReader((response.getEntity().getContent())));
	//	 
	//			String output;
	//			System.out.println("Output from Server .... \n");
	//			while ((output = br.readLine()) != null) {
	//				System.out.println(output);
	//			}
	//	 
	//			httpClient.getConnectionManager().shutdown();
	//	 
	//		  } catch (MalformedURLException e) {
	//	 
	//			e.printStackTrace();
	//	 
	//		  } catch (IOException e) {
	//	 
	//			e.printStackTrace();
	//	 
	//		  }
	//	}

}