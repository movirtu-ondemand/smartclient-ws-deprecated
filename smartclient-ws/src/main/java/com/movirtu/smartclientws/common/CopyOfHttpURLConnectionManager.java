package com.movirtu.smartclientws.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.Logger;


public class CopyOfHttpURLConnectionManager {

	private static Logger logger = Logger.getLogger(CopyOfHttpURLConnectionManager.class);

	/**
	 * This method sends Http get requests
	 * @throws Exception
	 */
	public String sendGet(String url) throws Exception {
		logger.debug("sendGet http get url=" + url);
		String httpResponse = null;
		long init = System.currentTimeMillis();

		try {
			URL httpUrl = new URL(url);

			HttpURLConnection connection = (HttpURLConnection)httpUrl.openConnection();
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(30000);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Accept", "application/json");

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String inputLine;
			StringBuffer responseBuffer = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				responseBuffer.append(inputLine);
			}
			httpResponse = responseBuffer.toString();
			in.close();

		}
		catch (IOException ex) {
			logger.error("Timed out after "  +(init - System.currentTimeMillis()) +" Error " , ex);
			throw new Exception("Timeout exception "+ex);
		}

		return httpResponse;
	}

	/**
	 * This method sends Http post requests
	 * @throws Exception
	 */

	public String sendPost(String url) throws Exception {

		logger.debug("sendGet http post url=" + url);
		String httpResponse = null;
		long init = System.currentTimeMillis();

		try {
			URL httpUrl = new URL(url);

			HttpURLConnection connection = (HttpURLConnection)httpUrl.openConnection();
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setConnectTimeout(30000);
			connection.setReadTimeout(30000);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Accept", "application/json");

			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String inputLine;
			StringBuffer responseBuffer = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				responseBuffer.append(inputLine);
			}
			httpResponse = responseBuffer.toString();
			in.close();

		}
		catch (IOException ex) {
			logger.error("Timed out after "  +(init - System.currentTimeMillis()) +" Error " , ex);
			throw new Exception("Timeout exception "+ex);
		}

		return httpResponse;

	}

}