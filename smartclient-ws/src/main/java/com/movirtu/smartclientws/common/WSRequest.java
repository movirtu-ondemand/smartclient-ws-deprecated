/*
 * To change this template; choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.common;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author david
 */
@XmlRootElement
public class WSRequest {
    String method;
    Params params;
    String id;
    String jsonrpc;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJsonrpc() {
        return jsonrpc;
    }

    public void setJsonrpc(String jsonrpc) {
        this.jsonrpc = jsonrpc;
    }
    
}
