package com.movirtu.smartclientws.common;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "subscriberDetails")
public class SubscriberDetails {

	private String firstname;
	private String lastname;
	private String sim_number;
	private String phonestate;
	private String notes;
	private String email;

	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getSim_number() {
		return sim_number;
	}
	public void setSim_number(String sim_number) {
		this.sim_number = sim_number;
	}
	public String getPhonestate() {
		return phonestate;
	}
	public void setPhonestate(String phonestate) {
		this.phonestate = phonestate;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


}
