package com.movirtu.smartclientws.common;
import java.util.HashMap;
import java.util.Map;



public enum ProfileState {

	ON(1),
	OFF(2),
	INACTIVE(3);

	int state=-1;
	private static Map<Integer, ProfileState> map = new HashMap<Integer, ProfileState>();

	static {
		for (ProfileState profileState : ProfileState.values()) {
			map.put(profileState.state, profileState);
		}
	}

	ProfileState(int state) {
		this.state = state;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public static ProfileState valueOf(int profileState) {
		return map.get(profileState);
	}
}
