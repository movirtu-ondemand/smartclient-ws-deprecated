package com.movirtu.smartclientws.common;

public enum ErrorCode {

	GENERIC_ERROR(501), 
	SUBSCRIBER_NOT_FOUND(502),
	INVALID_SUBSCRIBER(503),
	INVALID_PASSWORD(504),
	PROFILE_NOT_FOUND(505),
	VIRTUAL_NUMBER_NOT_AVAILABLE(506),
	MAX_PROFILE_LIMIT_REACHED(507),
	SIM_NOT_ALLWED(508),
	INVALID_TOPUP_VOUCHER(509),
	GENERIC_TOPUP_ERROR(510),
	INVALID_FTN(511),
	VERIFICATION_RETRIES_MAX_REACHED(512),
	INVALID_VERIFICATION_CODE(513),
	SYSTEM_FAILURE(514),
	CODE_EXPIRED(515),
	IMEI_CHANGED(516),
	INVALID_SIM(517),
	INVALID_REQUEST(518);


	private int errorCode;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	ErrorCode(int errorCode) {
		this.errorCode =errorCode;
	}
}
