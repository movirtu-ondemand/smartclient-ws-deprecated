package com.movirtu.smartclientws.common;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "profileDetails")
public class ProfileDetails {
	
	private String vsim_number;
	private String state;
	
	public String getVsim_number() {
		return vsim_number;
	}
	public void setVsim_number(String vsim_number) {
		this.vsim_number = vsim_number;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
}
