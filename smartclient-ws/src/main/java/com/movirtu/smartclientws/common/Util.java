package com.movirtu.smartclientws.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.entity.StringEntity;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.movirtu.vsp.persistence.domain.SubscriberInfo;

public class Util {

	private static RandomString cookeePwdGenerator = new RandomString(8);
	private static RandomString pinGenerator = new RandomString(4);
	public static final int VERIFY_RETRY_MAX=3;
	public static final int PROFILE_MAX=5;
	public static final String DEFAULT_PROFILE_COLOR = "rgb(64,96,191)";
	public static final int DEFAULT_PROFILE_ICON = 0;
	public static final String DEFAULT_PASSWORD="123456";
	public static final long CODE_EXPIRY_TIME = 24*60*60*1000;
	public static final String DEFAULT_FTN = "";
	public static final String DEFAULT_FORWARD_OPTION = "0000";	

	public static String generateCookeePassword() {
		return cookeePwdGenerator.nextString();
	}

	public static String generateValidationPin() {
		return "1234";
		//return pinGenerator.nextString();
	}

	//	public static String[] makeNumberList(List<String> numberList) throws JSONException {
	//		String[] arr = new String[numberList.size()];
	//		int index=0;
	//
	//		for (String number :numberList) {
	//			arr[index] = number;
	//			index++;
	//		}
	//
	//		return arr;
	//	}

	//	public static String[] makeDownloadData(List<SubscriberInfo> subscriberList) throws JSONException {
	//		String[] arr = new String[numberList.size()];
	//		int index=0;
	//
	//		for (String number :numberList) {
	//			arr[index] = number;
	//			index++;
	//		}
	//
	//		return arr;
	//	}


	public static JSONArray makeNumberList(List<String> numberList) {
		JSONArray jsonArray = new JSONArray();

		for (String number :numberList) {
			jsonArray.add(number);
		}
		return jsonArray;
	}

	public static JSONObject makeUserProfile(SubscriberInfo subscriberInfo) {
		JSONObject object = new JSONObject();
		//object.put(key, value)


		return object;
	}

}
