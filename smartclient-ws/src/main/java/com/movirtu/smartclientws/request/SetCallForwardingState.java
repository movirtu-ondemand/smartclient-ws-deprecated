package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"profile_prefix", "result"})

public class SetCallForwardingState extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SetCallForwardingState.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)new AppContext().getDAO();

	@XmlTransient
	private String profile_number;
	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;
	@XmlTransient
	private String forward_option;
	
	private int	profile_prefix;

	public SetCallForwardingState(String profile_number, 
			int profile_prefix,
			String sim_number,
			String password,
			String forward_option) {

		super("p_resp_setCallForwardingState");
		this.profile_number=profile_number;
		this.profile_prefix=profile_prefix;
		this.sim_number=sim_number;
		this.password=password;
		this.forward_option = forward_option;

	}

	public int getProfile_prefix() {
		return profile_prefix;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}
		
		resultSet = service.setCallForwardingState(sim_number, profile_prefix, forward_option);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Set call forwarding state failed for "+sim_number + " due to "+errcode);
			return;
		}

		log.debug("SetCallForwardingState successful for "+sim_number +" result="+result);

		return;

	}

}
