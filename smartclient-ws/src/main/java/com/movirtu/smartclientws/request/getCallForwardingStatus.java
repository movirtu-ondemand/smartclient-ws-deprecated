package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"profile_prefix", "numb_state", "forward_option", "FTN"})

public class getCallForwardingStatus extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(getCallForwardingStatus.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();
	
	@XmlTransient
	private String profile_number;
	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;
	
	private int	profile_prefix;
	private String	numb_state;
	private String	forward_option;
	private String FTN;
	
	public getCallForwardingStatus(String profile_number, 
			int profile_prefix,
			String sim_number,
			String password,
			String FTN) {

		super("p_resp_callForwardingStatus");
		
		this.profile_number=profile_number;
		this.profile_prefix=profile_prefix;
		this.sim_number=sim_number;
		this.password=password;
		this.FTN = FTN;

	}

	public int getProfile_prefix() {
		return profile_prefix;
	}

	public String getNumb_state() {
		return numb_state;
	}

	public String getForward_option() {
		return forward_option;
	}

	public String getFTN() {
		return FTN;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.getCallForwardingStatus(sim_number, profile_prefix);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Fetch SMS failed for "+sim_number + " due to "+errcode);
			return;
		}

		log.debug("getCallForwardingStatus successful for "+sim_number +" result="+result);

		return;
	}

}
