/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;

import com.movirtu.smartclientws.common.ErrorCode;

/**
 *
 * @author david
 */

public class ResultSetWrapper {

	Result result;
	ErrorCode errorCode;
	Object object;

	private ResultSetWrapper() {

	}

	public ResultSetWrapper(Result res) {
		this.result = res;
	}

	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String toDebugString() {
		String dbgs = "(ResultSet: ";
		dbgs += "result=";
		dbgs += result;
		dbgs += " errorCode=";
		dbgs += errorCode;
		dbgs += " object=";
		dbgs += object;
		dbgs += ") ";
		return dbgs;
	}
}