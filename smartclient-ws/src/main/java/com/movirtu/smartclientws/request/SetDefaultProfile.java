/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */

@XmlType(propOrder = {"result", "profile_number"})

public class SetDefaultProfile extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SetDefaultProfile.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;
	
	private String profile_number;

	public SetDefaultProfile(String sim_number, String password, String profile_number) {
		super("g_resp_setDefaultProfile");
		this.sim_number=sim_number;
		this.password=password;
		this.profile_number=profile_number;

	}

	public String getProfile_number() {
		return profile_number;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.setDefaultProfile(sim_number, profile_number);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Set default profile failed for "+sim_number + " due to "+errcode);
			return;
		}
		

		log.debug("Authenticate successful for "+sim_number +" result="+result);

	}

}
