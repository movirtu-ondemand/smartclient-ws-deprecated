package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.common.Util;
import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"result", "profile_prefix"})

public class DeleteProfile extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DeleteProfile.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String profile_number;
	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;

	private int	profile_prefix;

	public DeleteProfile(String profile_number, 
			String sim_number,
			String password,
			int profile_prefix) {

		super("g_resp_deleteProfile");
		this.profile_number=profile_number;
		this.sim_number=sim_number;
		this.password=password;
		this.profile_prefix=profile_prefix;

	}

	public int getProfile_prefix() {
		return profile_prefix;
	}

	@Override
	public void process() {
		result = Result.Success;
		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.deleteProfile(sim_number, profile_prefix);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Delete profile failed for "+sim_number + " due to "+errcode);
			return;
		}


		log.debug("Create new profile not implemented yet");

	}

}
