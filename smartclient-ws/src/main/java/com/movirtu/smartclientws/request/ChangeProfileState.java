/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.common.ProfileState;
import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"result", "profile_prefix"})

public class ChangeProfileState extends Resp {

	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(ChangeProfileState.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;
	@XmlTransient
	private int numb_state;
	private int profile_prefix;

	public ChangeProfileState(int profile_prefix, String sim_number,
			String password, int numb_state) {
		super("p_resp_changeProfileState");
		this.profile_prefix=profile_prefix;
		this.sim_number=sim_number;	
		this.password=password;
		this.numb_state=numb_state;
	}

	public int getProfile_prefix() {
		return profile_prefix;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			logger.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.changeProfileState(sim_number, profile_prefix, ProfileState.valueOf(numb_state));

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			logger.error("Fetch SMS failed for "+sim_number + " due to "+errcode);
			return;
		}

		service.updateProfileStateAtConsole(sim_number, profile_prefix, ProfileState.valueOf(numb_state));
		return;
	}

}
