/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = { "cmdjson","profile_prefix", "balance"})
public class CheckBalance extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CheckBalance.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)new AppContext().getDAO();

	private int profile_prefix;
	private String sim_number;
	private String password;
	private String profile_number;
	private Double balance;
	@XmlTransient
	String output;

	public CheckBalance(int profile_prefix, String sim_number,
			String password, String profile_number) {
		super("p_resp_balance");
		this.profile_prefix=profile_prefix;
		this.sim_number=sim_number;
		this.password=password;
		this.profile_number=profile_number;
	}

	@Override
	public void process() {
		result = Result.Success;
		
		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.getBalance(profile_prefix, sim_number, password, profile_number);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Fetch SMS failed for "+sim_number + " due to "+errcode);
			return;
		}

		balance = (Double) resultSet.getObject();

		
		log.debug("After Req-> fetch SMS, profile_prefix: "+profile_prefix + " ,sim_number: "+sim_number+" result="+result);
	}

}
