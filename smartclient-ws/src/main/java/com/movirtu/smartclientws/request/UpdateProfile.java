package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"result", "profile_prefix"})

public class UpdateProfile extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(UpdateProfile.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String profile_number;
	@XmlTransient
	private String password;
	@XmlTransient
	private String profile_name;
	@XmlTransient
	private String profile_colour;
	@XmlTransient
	private int profile_icon;
	@XmlTransient
	private int numb_state;
	@XmlTransient
	private String FTN;
	@XmlTransient
	private String forward_option;

	private String	profile_prefix;

	public UpdateProfile(String profile_number, 
			String profile_name,
			String profile_colour,
			String sim_number,
			String password,
			int profile_icon,
			int numb_state,
			String FTN,
			String forward_option) {

		super("g_resp_update_profile_data");
		this.profile_number=profile_number;
		this.profile_name=profile_name;
		this.profile_colour=profile_colour;
		this.sim_number=sim_number;
		this.password=password;
		this.profile_icon=profile_icon;
		this.numb_state = numb_state;
		this.FTN = FTN;
		this.forward_option=forward_option;

	}

	public String getProfile_prefix() {
		return profile_prefix;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.updateProfile(sim_number, profile_number, password, profile_name, profile_colour, profile_icon, numb_state, FTN, forward_option);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("update profile failed for "+sim_number + " due to "+errcode);
			return;
		}

		SubscriberInfo subscriber = (SubscriberInfo) resultSet.getObject();
		this.profile_prefix = ""+subscriber.getPrefix();
		log.debug("Leaving update profile for sim_number="+sim_number +" with user result="+result);

	}

}
