/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.common.UserState;
import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"result", "user_state"})

public class VerifyCode extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(VerifyCode.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)new AppContext().getDAO();

	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String verifyCode;
	@XmlTransient
	private String password;

	private String user_state;

	public VerifyCode(String sim_number, String verifyCode, String password) {
		super("g_resp_verify_code");
		this.sim_number=sim_number;
		this.verifyCode=verifyCode;
		this.password=password;		

	}

	public String getUser_state() {
		return user_state;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.isCodeApplicable(sim_number);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("verify sim failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.VerifyCode(sim_number, verifyCode);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Temp code verification failed for "+sim_number + " due to "+errcode);
			return;
		}

		//resultSet = service.changeProfileState(sim_number, UserState.CODE_VERIFIRED);
		SubscriberInfo subscriber = (SubscriberInfo) resultSet.getObject();
		user_state = ""+subscriber.getUser_state();

		service.updateSubscriberState(subscriber, UserState.CODE_VERIFIRED);

		service.updateSubscriberStateAtConsole(sim_number, subscriber.getPrefix(), UserState.AUTHENTICATED);

		log.debug("Verify code successful for "+sim_number +" result="+result);

	}

}
