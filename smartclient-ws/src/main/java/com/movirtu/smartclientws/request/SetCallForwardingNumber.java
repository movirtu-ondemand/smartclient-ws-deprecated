package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"profile_prefix", "result"})

public class SetCallForwardingNumber extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SetCallForwardingNumber.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String profile_number;
	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;
	@XmlTransient
	private String FTN;

	private int	profile_prefix;

	public SetCallForwardingNumber(String profile_number, 
			int profile_prefix,
			String sim_number,
			String password,
			String FTN) {

		super("p_resp_setCallForwardingNumber");
		this.profile_number=profile_number;
		this.profile_prefix=profile_prefix;
		this.sim_number=sim_number;
		this.password=password;
		this.FTN = FTN;

	}

	public int getProfile_prefix() {
		return profile_prefix;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}
		resultSet = service.setCallForwardingNumber(sim_number, profile_prefix, FTN);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Set call forwarding failed for "+sim_number + " due to "+errcode);
			return;
		}

		log.debug("SetCallForwardingNumber successful for "+sim_number +" result="+result);

		return;
	}

}
