package com.movirtu.smartclientws.request;


import java.util.List;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;

/**
 *
 * @author Prashant
 */

@XmlType(propOrder = {"preferred_name", "sim_number", "profile_list", "user_state", "network_prefix", "currency_code", "vm_number"})
public class DownloadDataJson extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DownloadDataJson.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String password;	
	private String preferred_name;
	private String sim_number;
	private JSONArray profile_list;
	private String user_state;	
	private String network_prefix ="999";
	private String currency_code ="£";
	private String vm_number="9999999999";


	public DownloadDataJson(String sim_number, String password	) {
		super("g_resp_download_data");

		this.sim_number=sim_number;
		this.password=password;

	}

	public String getPreferred_name() {
		return preferred_name;
	}

	public String getSim_number() {
		return sim_number;
	}

	public JSONArray getProfile_list() {
		return profile_list;
	}

	public String getUser_state() {
		return user_state;
	}

	public String getNetwork_prefix() {
		return network_prefix;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public String getVm_number() {
		return vm_number;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		log.debug("Download data not fully implemented");
		resultSet = service.getDefaultProfile(sim_number);
		SubscriberInfo subscriber = (SubscriberInfo) resultSet.getObject();
		user_state = ""+subscriber.getUser_state();
		preferred_name = "TMP-NAME";

		resultSet = service.getAllAssociatedProfile(sim_number);
		List<SubscriberInfo> subscriberList = (List<SubscriberInfo>) resultSet.getObject();

		JSONArray array = new JSONArray();
		try {		
			for(SubscriberInfo subs:subscriberList) {
				JSONObject object = new JSONObject();
				object.put("profile_number", subs.getProfile_number());
				object.put("profile_name",subs.getProfile_name());
				object.put("profile_prefix",subs.getPrefix());
				object.put("numb_state", subs.getNumb_state());
				object.put("forward_option","0000");
				object.put("profile_colour",subs.getProfile_color());
				object.put("profile_icon",subs.getProfile_icon());
				object.put("ftn","");
				object.put("isPrimary",subs.isPrimary());

				array.put(object);
			}
		} catch (JSONException e) {
			log.error("Exception in creating profile list",e);		
		}

		//profile_list = array.toString();
		profile_list = array;

		log.debug("Download data successful for "+sim_number +" result="+result +" profile_list="+profile_list);

	}

}
