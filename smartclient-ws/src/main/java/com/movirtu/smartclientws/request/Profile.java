/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;

import com.movirtu.smartclientws.common.ProfileState;



/**
 *
 * @author david
 */
public class Profile {

	private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Profile.class);

	//@XmlTransient
	private String profile_number;
	//@XmlTransient
	private String profile_name;
	//@XmlTransient
	private String profile_colour;
	//@XmlTransient
	private String profile_icon;
	//@XmlTransient
	private String profile_prefix;
	//@XmlTransient
	private String numb_state;
	//@XmlTransient
	private String ftn;
	//@XmlTransient
	private String forward_option;
	//@XmlTransient
	private String isPrimary;

	//private JSONObject profile = new JSONObject();

	//	private void setProperty(String key, String value) {
	//		profile.put(key,value);
	//	}

	public void setProfile_number(String profile_number) {
		this.profile_number = profile_number;
		//setProperty("profile_number", profile_number);
	}

	public void setProfile_name(String profile_name) {
		this.profile_name = profile_name;
		//setProperty("profile_name", profile_name);
	}

	public void setProfile_colour(String profile_colour) {
		this.profile_colour = profile_colour;
		//setProperty("profile_colour", profile_colour);
	}

	public void setProfile_icon(String profile_icon) {
		this.profile_icon = profile_icon;
		//setProperty("profile_icon", profile_icon);
	}

	public void setProfile_prefix(String profile_prefix) {
		this.profile_prefix = profile_prefix;
		//setProperty("profile_prefix", profile_prefix);
	}

	public void setNumb_state(String numb_state) {
		this.numb_state = numb_state;
		//setProperty("numb_state", numb_state);
	}

	public void setFtn(String ftn) {
		this.ftn = ftn;
		//setProperty("ftn", ftn);
	}

	public void setForward_option(String forward_option) {
		this.forward_option = forward_option;
		//setProperty("forward_option", forward_option);
	}
	public void setIsPrimary(String isPrimary) {
		this.isPrimary = isPrimary;
		//setProperty("isPrimary", isPrimary);
	}

	//@XmlTransient
	public String getProfile_number() {
		return this.profile_number;
	}
	//@XmlTransient
	public String getProfile_name() {
		return this.profile_name;
	}
	//@XmlTransient
	public String getProfile_colour() {
		return this.profile_colour;
	}
	//@XmlTransient
	public String getProfile_icon() {
		return this.profile_icon;
	}
	//@XmlTransient
	public String getProfile_prefix() {
		return this.profile_prefix;
	}
	//@XmlTransient
	public String getNumb_state() {
		return this.numb_state;
	}
	//@XmlTransient
	public String getFtn() {
		return this.ftn;
	}
	//@XmlTransient
	public String getForward_option() {
		return this.forward_option;
	}
	//@XmlTransient
	public String getIsPrimary() {
		return this.isPrimary;
	}
	//	//@XmlTransient
	//	public JSONObject getProfile() {
	//		return this.profile;
	//	}
	//	public void setProfile(JSONObject profile) {
	//		this.profile = profile;
	//	}



}
