/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"result", "user_state", "verify_retry_max", "profile_max", "password"})

public class SignUp extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SignUp.class);
	public static final String DEFAULT_PROFILE_NAME="Primary SIM";
	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)new AppContext().getDAO();

	@XmlTransient
	private String preferred_name;
	@XmlTransient
	private String sim_number;
	private String user_state;
	private String verify_retry_max;
	private String profile_max;
	private String password;
	private String	profile_prefix;
	
	public SignUp(String preferred_name, String sim_number) {
		super("g_resp_signup");
		this.preferred_name=preferred_name;
		this.sim_number=sim_number;
	}

	public SignUp(String sim_number) {
		super("g_resp_signup");
		this.sim_number=sim_number;
	}

	public String getPassword() {
		return password;
	}

	public String getUser_state() {
		return user_state;
	}

	public String getVerify_retry_max() {
		return verify_retry_max;
	}

	public String getProfile_max() {
		return profile_max;
	}

	public String getProfile_prefix() {
		return profile_prefix;
	}

	@Override
	public void process() {
		result = Result.Success;

		//ResultSetWrapper resultSet = service.signUp(preferred_name, sim_number);
		ResultSetWrapper resultSet = service.signUp(sim_number);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Sign Up failed for "+sim_number + " due to "+errcode);
			return;
		}

		// Need to change this to UserProfile.
		SubscriberInfo subscriberInfo = (SubscriberInfo) resultSet.getObject();

		user_state = ""+subscriberInfo.getUser_state();
		verify_retry_max = ""+subscriberInfo.getVerify_retry_max();
		profile_max = ""+subscriberInfo.getProfile_max();
		password = subscriberInfo.getCookeepassword();
		profile_prefix = ""+subscriberInfo.getPrefix();
		log.debug("After Req-> Signup, preferred_name: "+preferred_name + " ,sim_number: "+sim_number+" result="+result);
	}

}
