package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"profile_prefix", "balance"})

public class BalanceCheck extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(BalanceCheck.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String profile_number;
	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;
	@XmlTransient
	private int	profile_prefix_int;

	private String profile_prefix;
	private String balance;

	public BalanceCheck(String profile_number, 
			int profile_prefix,
			String sim_number,
			String password) {

		super("p_resp_balance");

		this.profile_number=profile_number;
		this.profile_prefix_int=profile_prefix;
		this.profile_prefix = ""+profile_prefix;
		this.sim_number=sim_number;
		this.password=password;

	}

	public String getProfile_prefix() {
		return profile_prefix;
	}

	public String getBalance() {
		return balance;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.getBalance(profile_prefix_int, sim_number, password, profile_number);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Balance failed for "+sim_number + " due to "+errcode);
			return;
		}
		
		balance = ""+(Double) resultSet.getObject();

		log.debug("Balance query successful for "+sim_number +" result="+result);

	}

}
