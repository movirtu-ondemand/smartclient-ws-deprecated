package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"profile_prefix", "result", "balance"})

public class TopUp extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(TopUp.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)new AppContext().getDAO();

	@XmlTransient
	private String profile_number;
	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;

	private int	profile_prefix;
	private Double balance;

	public TopUp(String profile_number, 
			int profile_prefix,
			String sim_number,
			String password) {

		super("p_resp_topup");
		this.profile_number=profile_number;
		this.profile_prefix=profile_prefix;
		this.sim_number=sim_number;
		this.password=password;

	}

	public int getProfile_prefix() {
		return profile_prefix;
	}

	public Double getBalance() {
		return balance;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		log.debug("Topup not implemented yet");
		balance = 45.0;

	}

}
