package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.common.UserState;
import com.movirtu.smartclientws.common.Util;
import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"result", "profile_prefix"})

public class CreateProfile extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(CreateProfile.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String profile_number;
	@XmlTransient
	private String profile_name;
	@XmlTransient
	private String profile_colour;
	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;
	@XmlTransient
	private int profile_icon_int;
	@XmlTransient
	private String profile_icon;
	@XmlTransient
	private int	profile_prefix_int;
	private String	profile_prefix;
	@XmlTransient
	private String imsi;
	@XmlTransient
	private boolean is_internal;

	public CreateProfile(String profile_number, 
			String profile_name,
			String profile_colour,
			String sim_number,
			int profile_prefix_int,
			String password,
			int profile_icon_int,
			String imsi,
			boolean is_internal) {

		super("g_resp_createNewProfile");
		this.profile_number=profile_number;
		this.profile_name=profile_name;
		this.profile_colour=profile_colour;
		this.sim_number=sim_number;
		this.profile_prefix_int = profile_prefix_int;
		this.password=password;
		this.profile_icon_int=profile_icon_int;
		this.imsi = imsi;
		this.is_internal = is_internal;
	}

	public String getProfile_prefix() {
		return profile_prefix;
	}

	@Override
	public void process() {
		result = Result.Success;
		log.debug("Inside create profile action");
		// TODO temp code as worklife console can not send us password
		ResultSetWrapper resultSet=null;

		if(Util.DEFAULT_PASSWORD.equals(password)) {
			log.debug("Default password matched");
		} else {
			// temp code ends
			resultSet = service.AuthenticateUser(sim_number, password);

			if(resultSet.getResult() == Result.FAIL) {
				result = Result.FAIL;
				errcode = ""+resultSet.getErrorCode().getErrorCode();
				log.error("Authenticate failed for "+sim_number + " due to "+errcode);
				return;
			}
		}

		//		resultSet = service.canCreateVirtualProfile(sim_number, is_internal);
		//
		//
		//		if(resultSet.getResult() == Result.FAIL) {
		//			result = Result.FAIL;
		//			errcode = ""+resultSet.getErrorCode().getErrorCode();
		//			log.error("Create profile failed for "+sim_number + " due to "+errcode);
		//			return;
		//		}

		//		// This is done as for internal requests, password received is 
		//		// default password i.e. 123456. whereas for req received from 
		//		// mobile app it is cookee passowrd. if we create db entry for 
		//		// this profile with default password, two profile entries for 
		//		// this user will have different cookee password.
		//		// 
		//		resultSet = service.findPrimaryProfile(sim_number);
		//
		//		if(is_internal && resultSet.getResult() == Result.Success) {
		//			log.debug("Primary profile present for an internal request");
		//			SubscriberInfo subscroberInfo = (SubscriberInfo)resultSet.getObject();
		//			password = subscroberInfo.getCookeepassword();
		//		}

		// create virtual profile
		resultSet = service.createVirtualProfile (
				profile_number, 
				profile_name, 
				profile_colour, 
				sim_number, 
				//profile_prefix_int, 
				password, 
				profile_icon_int, 
				Util.PROFILE_MAX, 
				Util.VERIFY_RETRY_MAX, 
				Util.generateValidationPin(), 
				UserState.AUTHENTICATED.getState(),
				Util.DEFAULT_FTN,
				Util.DEFAULT_FORWARD_OPTION);

		if(resultSet.getResult()==Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Can not create profile for "+sim_number + " due to "+errcode);
			return;
		}

		SubscriberInfo subscriber = (SubscriberInfo) resultSet.getObject();
		profile_prefix = ""+subscriber.getPrefix();

		//Purchase number
		if(is_internal) {
			service.purchaseVirtualNumberInternal(sim_number, profile_number, imsi, subscriber.getPrefix());
		} else {
			service.purchaseVirtualNumber(sim_number, profile_number, subscriber.getPrefix());
		}


		log.debug("Profile creation successful for "+sim_number +" result="+result);
	}

}
