package com.movirtu.smartclientws.request;


import java.util.List;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;

/**
 *
 * @author Prashant
 */

@XmlType(propOrder = {"preferred_name", "sim_number", "user_state", "network_prefix", "currency_code", "vm_number", "profile_list"})
public class DownloadData extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(DownloadDataString.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String password;	
	private String preferred_name;
	private String sim_number;
	private JSONArray profile_list;
	private String user_state;	
	private String network_prefix ="999";
	private String currency_code ="£";
	private String vm_number="0800123412";


	public DownloadData(String sim_number, String password	) {
		super("g_resp_download_data");

		this.sim_number=sim_number;
		this.password=password;
		profile_list = new JSONArray();
	}

	public String getPreferred_name() {
		return preferred_name;
	}

	public String getSim_number() {
		return sim_number;
	}

	public JSONArray getProfile_list() {
		return profile_list;
	}

	public String getUser_state() {
		return user_state;
	}

	public String getNetwork_prefix() {
		return network_prefix;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public String getVm_number() {
		return vm_number;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		log.debug("Download data not fully implemented");
		resultSet = service.getDefaultProfile(sim_number);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Download data failed for "+sim_number + " due to "+errcode);
			return;
		}
		

		SubscriberInfo subscriber = (SubscriberInfo) resultSet.getObject();
		user_state = ""+subscriber.getUser_state();
		preferred_name = "TMP-NAME";

		resultSet = service.getAllAssociatedProfile(sim_number);
		List<SubscriberInfo> subscriberList = (List<SubscriberInfo>) resultSet.getObject();

		for(SubscriberInfo subs:subscriberList) {
			Profile profile = new Profile();
			profile.setProfile_number(subs.getProfile_number());
			profile.setProfile_name(subs.getProfile_name());
			profile.setProfile_prefix(""+subs.getPrefix());
			profile.setNumb_state(""+subs.getNumb_state());
			profile.setForward_option(subs.getForward_option());
			profile.setProfile_colour(subs.getProfile_color());
			profile.setProfile_icon(""+subs.getProfile_icon());
			profile.setFtn(subs.getFTN());
			profile.setIsPrimary(""+subs.isPrimary());

			profile_list.add(profile);
		}

		log.debug("Download data successful for "+sim_number +" result="+result +" profile_list="+profile_list);

	}

}
