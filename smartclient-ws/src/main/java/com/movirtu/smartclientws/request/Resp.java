/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author david
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Resp {

	Result result;
	String cmdjson;

	@XmlTransient
	public String errcode;

	Resp(String cmdjson){
		this.cmdjson = cmdjson;
	}

	public void process(){
	}

	public Result getResult() {
		return result;
	}

	//	public void setResult(Result result) {
	//		this.result = result;
	//	}

	public String getCmdjson() {
		return cmdjson;
	}

	public void setCmdjson(String cmdjson) {
		this.cmdjson = cmdjson;
	}

	public String toDebugString() {
		return "command="+cmdjson +
				" result="+result.toString() +
				" ErrorCode=" +errcode;
	}
}
