/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.request;

import javax.xml.bind.annotation.XmlType;

/**
 *
 * @author david
 */
@XmlType(propOrder = { "cmdjson", "result", "error_code"})
public class PError extends Resp{

	String error_code;

	public PError(String error_code, String cmdjson){
		super(cmdjson);
		this.error_code = error_code;
		result = Result.FAIL;
	}


	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	//    @XmlTransient
	//    @Override
	//    public String getProfile_prefix() {
		//        return profile_prefix;
	//    }
	//
	//    @XmlTransient
	//    @Override
	//    public void setProfile_prefix(String profile_prefix) {
	//        this.profile_prefix = profile_prefix;
	//    }
}
