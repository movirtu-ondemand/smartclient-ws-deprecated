package com.movirtu.smartclientws.request;


import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"reult", "user_state"})

public class GetUserState extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(GetUserState.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	@XmlTransient
	private String sim_number;
	@XmlTransient
	private String password;

	String user_state;

	public GetUserState(String sim_number, String password) {
		super("g_resp_get_user_state");

		this.sim_number=sim_number;
		this.password=password;

	}

	public String getUser_state() {
		return user_state;
	}

	@Override
	public void process() {
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.getUserState(sim_number);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("getUserState failed for "+sim_number + " due to "+errcode);
			return;
		}

		user_state = ""+((SubscriberInfo)resultSet.getObject()).getUser_state();
		log.debug("Leaving getUserState for sim_number="+sim_number +" with user state="+user_state);

	}

}
