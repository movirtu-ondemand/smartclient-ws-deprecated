package com.movirtu.smartclientws.request;


import java.util.List;

import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.json.simple.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;

import com.movirtu.smartclientws.common.Util;
import com.movirtu.smartclientws.service.AppContext;
import com.movirtu.smartclientws.service.SmartClientWebServiceHandler;

/**
 *
 * @author Prashant
 */
@XmlType(propOrder = {"sim_number", "number_list"})

public class NewNumbers extends Resp {

	private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(NewNumbers.class);

	@Autowired
	SmartClientWebServiceHandler service = (SmartClientWebServiceHandler)AppContext.getDAO();

	private String sim_number;
	@XmlTransient
	private String password;
	//public List<NewVirtaulNumber> number_list;
	private JSONArray number_list;

	public NewNumbers(String sim_number, String password) {
		super("g_resp_newNumbers");

		this.sim_number=sim_number;
		this.password=password;
	}

	public String getSim_number() {
		return sim_number;
	}

	//	public List<NewVirtaulNumber> getNumber_list() {
	public JSONArray getNumber_list() {
		return number_list;
	}

	@XmlTransient
	@Override
	public Result getResult() {
		return result;
	}

	@Override
	public void process() {
		log.debug("Inside get NewNumbers");
		result = Result.Success;

		ResultSetWrapper resultSet = service.AuthenticateUser(sim_number, password);

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("Authenticate failed for "+sim_number + " due to "+errcode);
			return;
		}

		resultSet = service.getPurchaseNumbers();

		if(resultSet.getResult() == Result.FAIL) {
			result = Result.FAIL;
			errcode = ""+resultSet.getErrorCode().getErrorCode();
			log.error("List available number failed for "+sim_number + " due to "+errcode);
			return;
		}

		List<String> numberList = (List<String>) resultSet.getObject();

		//		number_list = new LinkedList<NewVirtaulNumber>();
		//		for(String number: numberList) {
		//			NewVirtaulNumber pn = new NewVirtaulNumber();
		//			pn.setNumb(number);
		//			number_list.add(pn);
		//
		//		}

		number_list = Util.makeNumberList(numberList);

		log.debug("List of purchase number sent :: "+number_list);

	}

}
