package com.movirtu.smartclientws.router;

/**
 *
 * @author Prashant
 */

import javax.servlet.ServletConfig;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
//import javax.xml.ws.handler.MessageContext;

import org.apache.log4j.Logger;

import com.movirtu.smartclientws.request.AuthenticateUser;
import com.movirtu.smartclientws.request.BalanceCheck;
import com.movirtu.smartclientws.request.ChangeProfileState;
import com.movirtu.smartclientws.request.CreateProfile;
import com.movirtu.smartclientws.request.DeleteProfile;
import com.movirtu.smartclientws.request.DownloadData;
import com.movirtu.smartclientws.request.GetUserState;
import com.movirtu.smartclientws.request.NewNumbers;
import com.movirtu.smartclientws.request.PError;
import com.movirtu.smartclientws.request.Resp;
import com.movirtu.smartclientws.request.Result;
import com.movirtu.smartclientws.request.SetCallForwardingNumber;
import com.movirtu.smartclientws.request.SetCallForwardingState;
import com.movirtu.smartclientws.request.SetDefaultProfile;
import com.movirtu.smartclientws.request.SignUp;
import com.movirtu.smartclientws.request.TopUp;
import com.movirtu.smartclientws.request.UpdateProfile;
import com.movirtu.smartclientws.request.VerifyCode;
import com.movirtu.smartclientws.request.getCallForwardingStatus;

@Path("/request")
public class SmartClientWebService<T> {

	private static final Logger log = Logger.getLogger(SmartClientWebService.class);
	@Context ServletConfig sc;

	T proces(T result){
		Resp res = (Resp)result;
		res.process();
		if(res.getResult().equals(Result.FAIL)){
			PError err = new PError(res.errcode, res.getCmdjson());
			return (T)err;
		}

		return (T)res;
	}

	@POST
	@Path("/mclient")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")       
	public Resp postRequest(
			@QueryParam("cmdjson") String cmd, 
			@QueryParam("profile_prefix") String profile_prefix_str,
			@QueryParam("sim_number") String sim_number,
			@QueryParam("password") String password,
			@QueryParam("profile_number") String profile_number,
			@QueryParam("numb_state") String numb_state_str,
			@QueryParam("preferred_name") String preferred_name,
			@QueryParam("verify_code") String verify_code,
			@QueryParam("profile_name") String profile_name,
			@QueryParam("profile_colour") String profile_colour,
			@QueryParam("profile_icon") String profile_icon_str,
			@QueryParam("FTN") String FTN,
			@QueryParam("forward_option") String forward_option,
			@QueryParam("imsi") String imsi,
			@QueryParam("is_internal") String is_internal_str
			) {

		Resp res = null;
		log.debug("POST request >> [[ "+cmd 
				+" For profile_prefix: '"+profile_prefix_str
				+" , sim_number is : "+sim_number
				+" , password is : "+password
				+" , profile_number is : "+profile_number
				+" , numb_state is : "+numb_state_str
				+" , preferred_name is : "+preferred_name
				+" , verify_code is : "+verify_code
				+" , profile_name is : "+profile_name
				+" , profile_colour is : "+profile_colour
				+" , profile_icon is : "+profile_icon_str
				+" , FTN is : "+FTN
				+" , forward_option is : "+forward_option
				+" , imsi is : "+imsi
				+" , is_internal is : "+is_internal_str

				+"]]");
		try {
			int profile_prefix=-1;
			int numb_state=-1;
			int profile_icon = -1;
			boolean is_internal=false;

			if(profile_prefix_str != null) {
				profile_prefix = Integer.parseInt(profile_prefix_str);
			}

			if(numb_state_str != null) {
				numb_state = Integer.parseInt(numb_state_str);
			}

			if(profile_icon_str != null) {
				profile_icon = Integer.parseInt(profile_icon_str);
			}

			if(is_internal_str != null) {
				is_internal = Boolean.parseBoolean(is_internal_str);
			}

			if(cmd.equalsIgnoreCase("g_req_signup")){
				SignUp pss = new SignUp(preferred_name, sim_number);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("g_req_verify_code")) {
				VerifyCode pss = new VerifyCode(sim_number, verify_code, password);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("g_req_authenticate")) {
				AuthenticateUser pss = new AuthenticateUser(sim_number, password);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("g_req_createNewProfile")) {
				CreateProfile pss = new CreateProfile(profile_number, profile_name, profile_colour, sim_number, profile_prefix, password, profile_icon, imsi, is_internal);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("p_req_changeProfileState")) {     
				ChangeProfileState pss = new ChangeProfileState(profile_prefix, sim_number, password, numb_state);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("p_req_topup")){
				TopUp pss = new TopUp(profile_number, profile_prefix, sim_number, password);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("p_req_setCallForwardingNumber")) {
				SetCallForwardingNumber pss = new SetCallForwardingNumber(profile_number, profile_prefix, sim_number, password, FTN);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("p_req_setCallForwardingState")) {
				SetCallForwardingState pss = new SetCallForwardingState(profile_number, profile_prefix, sim_number, password, forward_option);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("g_req_deleteProfile")){
				DeleteProfile pss = new DeleteProfile(profile_number, sim_number, password, profile_prefix);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("g_req_update_profile_data")) {
				UpdateProfile pss = new UpdateProfile(profile_number, profile_name, profile_colour, sim_number, password, profile_icon, numb_state, FTN, forward_option);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("g_req_setDefaultProfile")) {
				SetDefaultProfile pss = new SetDefaultProfile(sim_number, password, profile_number);
				res = (Resp)proces((T)pss);
				// TODO this should be in GET only. Temp code to test mobile client
				// need to update mobile client
			} else if(cmd.equalsIgnoreCase("g_req_download_data")) {
				DownloadData pss = new DownloadData(sim_number, password);
				res = (Resp)proces((T)pss);


			} else{
				PError err = new PError("518",cmd);
				err.errcode = "518";
				res = (Resp)proces((T)err);                    
			}

		}catch(Exception ex) {
			log.error("Exception in processing command="+cmd ,ex);
			PError err = new PError("501",cmd);
			err.errcode = "501";
			res = (Resp)proces((T)err);                    

		}

		log.debug("Wins : "+cmd+" ,POST respCmd->"+res.getCmdjson() +" resp="+res.toDebugString());
		return res;
	}


	@GET
	@Path("/mclient")
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")       
	public Resp getRequest(
			@QueryParam("cmdjson") String cmd, 
			@QueryParam("profile_prefix") String profile_prefix_str,
			@QueryParam("sim_number") String sim_number,
			@QueryParam("password") String password,
			@QueryParam("profile_number") String profile_number,
			@QueryParam("numb_state") String numb_state_str,
			@QueryParam("FTN") String FTN
			) {              
		Resp res = null;
		log.debug("Wins GET request : "+cmd 
				+" For profile_prefix: '"+profile_prefix_str
				+" , sim_number is : "+sim_number
				+" , password is : "+password
				+" , profile_number is : "+profile_number
				+" , numb_state is : "+numb_state_str
				+" , FTN is : "+FTN
				);
		try {
			int profile_prefix=-1;

			if(profile_prefix_str != null) {
				profile_prefix = Integer.parseInt(profile_prefix_str);
			}

			//			if(cmd.equalsIgnoreCase("p_req_balance")) {     
			//				CheckBalance pss = new CheckBalance(profile_prefix, sim_number, password, profile_number);
			//				res = (Resp)proces((T)pss);
			//
			//			} else if(cmd.equalsIgnoreCase("g_req_download_data")) {
			if(cmd.equalsIgnoreCase("g_req_download_data")) {
				DownloadData pss = new DownloadData(sim_number, password);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("g_req_newNumbers")) {
				NewNumbers pss = new NewNumbers(sim_number, password);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("p_req_balance")) {
				BalanceCheck pss = new BalanceCheck(profile_number, profile_prefix, sim_number, password);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("p_req_callForwardingStatus")) {
				getCallForwardingStatus pss = new getCallForwardingStatus(profile_number, profile_prefix, sim_number, password, FTN);
				res = (Resp)proces((T)pss);

			} else if(cmd.equalsIgnoreCase("g_req_get_user_state")) {
				GetUserState pss = new GetUserState(sim_number, password);
				res = (Resp)proces((T)pss);

			}else{
				PError err = new PError("518",cmd);
				err.errcode = "518";
				res = (Resp)proces((T)err);                    
			}
		}catch(Exception ex) {
			log.error("Exception in processing command="+cmd ,ex);
		}


		log.debug("Wins : "+cmd+" ,GET respCmd->"+res.getCmdjson() +" resp="+res.toDebugString());
		return res;
	}

}
