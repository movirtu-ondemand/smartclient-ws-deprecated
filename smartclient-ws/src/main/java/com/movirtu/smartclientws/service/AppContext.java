/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.movirtu.smartclientws.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationContextException;
import org.springframework.stereotype.Component;



/**
 *
 * @author david
 */

@Component
public class AppContext implements ApplicationContextAware {
	
	private static SmartClientWebServiceHandler nuDao = null;
	
	public static SmartClientWebServiceHandler getDAO() {
		if(nuDao == null){
			throw new ApplicationContextException("context not initialized");
		}
		return nuDao;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		nuDao = applicationContext.getBean(SmartClientWebServiceHandler.class);
	}

}