/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.movirtu.smartclientws.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.movirtu.map.MapJavaThriftHandler;
import com.movirtu.map.message.LUResponse;
import com.movirtu.map.message.MsrnRequest;
import com.movirtu.map.message.MsrnResponse;
import com.movirtu.map.message.SRISMRequest;
import com.movirtu.map.message.SRISMResponse;
import com.movirtu.smartclientws.common.ErrorCode;
import com.movirtu.smartclientws.common.HttpURLConnectionManager;
import com.movirtu.smartclientws.common.Params;
import com.movirtu.smartclientws.common.ProfileDetails;
import com.movirtu.smartclientws.common.ProfileState;
import com.movirtu.smartclientws.common.SubscriberDetails;
import com.movirtu.smartclientws.common.UserState;
import com.movirtu.smartclientws.common.Util;
import com.movirtu.smartclientws.request.Result;
import com.movirtu.smartclientws.request.ResultSetWrapper;
import com.movirtu.smartclientws.request.SignUp;
import com.movirtu.vsp.core.service.VspCoreServiceImpl;
import com.movirtu.vsp.persistence.dao.hb.ImsiMsrnMappingDao;
import com.movirtu.vsp.persistence.dao.hb.SettingsDao;
import com.movirtu.vsp.persistence.dao.hb.SubscriberInfoDao;
import com.movirtu.vsp.persistence.domain.ImsiMsrnMapping;
import com.movirtu.vsp.persistence.domain.SubscriberInfo;
import com.movirtu.vsp.persistence.exceptions.RecordNotFoundException;
/**
 *
 * @author Prashant
 */
@Component
public class SmartClientWebServiceHandler implements Observer {

	private static final Logger logger = Logger.getLogger(SmartClientWebServiceHandler.class);

	private ExecutorService executor = Executors.newFixedThreadPool(1);
	@Autowired
	private ImsiMsrnMappingDao imsiMsrnMappingDao; 
	@Autowired
	private SubscriberInfoDao subscriberInfoDao;
	@Autowired
	private SettingsDao settingDao;

	@Autowired
	VspCoreServiceImpl vspCoreService;
	private ScheduledExecutorService msrnReleaseTaskExecutor = Executors.newSingleThreadScheduledExecutor();

	public SmartClientWebServiceHandler() {		
		MapJavaThriftHandler.getInstance().addObserver(this);
	}

	// This is a callback method for JavaServerImpl. For all the thrift messages received
	// from mapgw, this method should get called. This may be redundant in current implementation
	// as most of the service logic is handled by JavaServerImpl class.
	@Override
	public void update(Observable observer, Object message) {
		logger.debug("Entering update with message = {}"+ message);

		if ((message instanceof SRISMResponse)) {
			handleSRISMResponse((SRISMResponse) message);
		} else if ((message instanceof MsrnRequest)) {
			handleMsrnRequest((MsrnRequest) message);
		} else {
			logger.warn("This message is not of desired type: {}" + message);
		}

	}

	/**
	 * This method handles SRISM response received from mapgw.
	 * @param message SRISMResponse message received from network.
	 */
	private void handleSRISMResponse(SRISMResponse message) {
		logger.debug("Inside handleSRISMResponse");
		vspCoreService.handleSRISMResponse(message);
		sendValidationSMS(message.getMsisdn(), "1234");
	}

	/**
	 * This method handles Location update response received from mapgw.
	 * @param message Location update response message received from network.
	 */
	private void handleLUResponse(LUResponse luResponse) {
		logger.debug("Inside handleLUResponse");

		String imsi = luResponse.getImsi();
		String msisdn = luResponse.getMsisdn();

	}

	/*
	 *	Temporary method to provide msrn
	 */
	private String getMsrn() {
		return msrnList.remove(0);
	}

	/*
	 *	Temporary method to release msrn
	 */
	private void releaseMsrn(String msrn) {
		deleteMsrnMapping(msrn);
		msrnList.add(msrn);
	}

	/**
	 * This method handles MSRN request received from mapgw.
	 * @param msrnRequest MSRN request message received from network.
	 */
	private void handleMsrnRequest(MsrnRequest msrnRequest) {
		logger.debug("Inside handleMsrnRequest");
		MsrnResponse msrnResponse;
		try {
			msrnResponse = (MsrnResponse) msrnRequest.createResponse();
		} catch (Exception ex) {	
			logger.error("Exception in creating MsrnResponse ",ex);
			return;
		}

		String msrn = getMsrn();

		deleteMsrnMapping(msrn);
		createMsrnMapping(msrn, msrnRequest.getImsi());
		// TODO temp Adding country code 255 
		msrnResponse.setMsrn("255"+msrn);
		msrnResponse.setError((short) 0);
		// submitting a timer task to release the msrn after some time.
		msrnReleaseTaskExecutor.schedule(new MsrnReleaseTimerTask(msrn), 60, TimeUnit.SECONDS );
		MapJavaThriftHandler.getInstance().submit(msrnResponse);
	}

	private void createMsrnMapping(String msrn, String imsi) {
		ImsiMsrnMapping mapping = new ImsiMsrnMapping();
		mapping.setImsi(imsi);
		mapping.setMsrn(msrn);
		imsiMsrnMappingDao.addNew(mapping);
	}

	private void deleteMsrnMapping(String msrn) {
		try {
			ImsiMsrnMapping imsiMsrnMapping = imsiMsrnMappingDao.findByMsrn(msrn);
			if(imsiMsrnMapping!=null) {
				imsiMsrnMappingDao.delete(imsiMsrnMapping);
			}
		} catch (RecordNotFoundException e) {
			logger.debug("msrn mapping not present");
		}
	}

	class MsrnReleaseTimerTask extends TimerTask {

		String msrn;
		long sessionId;

		MsrnReleaseTimerTask(String msrn) {
			this.msrn = msrn;
		}

		@Override
		public void run() {
			logger.error("Releasing msrn="+msrn);
			releaseMsrn(msrn);
			logger.debug("Msrn released successfully");
		}

	}

	/////////// TEMP CODE STARTS
	private static List<String> msrnList = new ArrayList<String>();
	static{
		msrnList.add("798034000");
		msrnList.add("798034001");
		msrnList.add("798034002");
		msrnList.add("798034003");
		msrnList.add("798034004");
		msrnList.add("798034005");
		msrnList.add("798034006");
		msrnList.add("798034007");
		msrnList.add("798034008");
		msrnList.add("798034009");
		msrnList.add("798034010");
		msrnList.add("798034011");
		msrnList.add("798034012");
		msrnList.add("798034013");
		msrnList.add("798034014");
		msrnList.add("798034015");
		msrnList.add("798034016");
		msrnList.add("798034017");
		msrnList.add("798034018");
		msrnList.add("798034019");
		msrnList.add("798034020");
		msrnList.add("798034021");
		msrnList.add("798034022");
		msrnList.add("798034023");
		msrnList.add("798034024");
		msrnList.add("798034025");
		msrnList.add("798034026");
		msrnList.add("798034027");
		msrnList.add("798034028");
		msrnList.add("798034029");
		msrnList.add("798034030");
		msrnList.add("798034031");
		msrnList.add("798034032");
		msrnList.add("798034033");
		msrnList.add("798034034");
		msrnList.add("798034035");
		msrnList.add("798034036");
		msrnList.add("798034037");
		msrnList.add("798034038");
		msrnList.add("798034039");
		msrnList.add("798034040");
		msrnList.add("798034041");
		msrnList.add("798034042");
		msrnList.add("798034043");
		msrnList.add("798034044");
		msrnList.add("798034045");
		msrnList.add("798034046");
		msrnList.add("798034047");
		msrnList.add("798034048");
		msrnList.add("798034049");
		msrnList.add("798034050");
		msrnList.add("798034051");
		msrnList.add("798034052");
		msrnList.add("798034053");
		msrnList.add("798034054");
		msrnList.add("798034055");
		msrnList.add("798034056");
		msrnList.add("798034057");
		msrnList.add("798034058");
		msrnList.add("798034059");
		msrnList.add("798034060");
		msrnList.add("798034061");
		msrnList.add("798034062");
		msrnList.add("798034063");
		msrnList.add("798034064");
		msrnList.add("798034065");
		msrnList.add("798034066");
		msrnList.add("798034067");
		msrnList.add("798034068");
		msrnList.add("798034069");
		msrnList.add("798034070");
		msrnList.add("798034071");
		msrnList.add("798034072");
		msrnList.add("798034073");
		msrnList.add("798034074");
		msrnList.add("798034075");
		msrnList.add("798034076");
		msrnList.add("798034077");
		msrnList.add("798034078");
		msrnList.add("798034079");
		msrnList.add("798034080");
		msrnList.add("798034081");
		msrnList.add("798034082");
		msrnList.add("798034083");
		msrnList.add("798034084");
		msrnList.add("798034085");
		msrnList.add("798034086");
		msrnList.add("798034087");
		msrnList.add("798034088");
		msrnList.add("798034089");
		msrnList.add("798034090");
		msrnList.add("798034091");
		msrnList.add("798034092");
		msrnList.add("798034093");
		msrnList.add("798034094");
		msrnList.add("798034095");
		msrnList.add("798034096");
		msrnList.add("798034097");
		msrnList.add("798034098");
		msrnList.add("798034099");
	}
	/////////// TEMP CODE ENDS 

	////////////////////////////////////////////////////////////////////////////////
	/////////////////// JSON SERVICE METHOD IMPLEMENTATION /////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ResultSetWrapper getBalance(int profile_prefix, String sim_number, String password, String profile_number) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside fetchBalace with profile_prefix="+profile_prefix +" and sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		if(logger.isDebugEnabled()) {
			logger.debug("Returning dummy value");
		}

		if(true) {
			result.setObject(5.0);
			return result;
		}

		result = getProfile(sim_number,profile_prefix);
		SubscriberInfo subscriber = (SubscriberInfo) result.getObject();

		double balance = vspCoreService.getBalance(subscriber.getProfile_number());
		result.setObject(balance);
		return result;

	}

	public ResultSetWrapper switchProflie(String profile_prefix,
			String sim_number, String password, String numb_state) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside switchProflie with profile_prefix="+profile_prefix +" and sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		boolean success = vspCoreService.setDefaultNumber(sim_number);

		if(success) {
			return result;
		}

		result.setResult(Result.FAIL);
		result.setErrorCode(ErrorCode.GENERIC_ERROR);
		return result;
	}

	public ResultSetWrapper findPrimaryProfile(String sim_number) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside findPrimaryProfile sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);
		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findPrimaryProfile(sim_number);
		} catch (RecordNotFoundException e) {
			logger.info("Primary profile not found "+e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.SUBSCRIBER_NOT_FOUND);
			return result;

		}


		logger.debug("Setting subscriber="+subscriber);
		result.setObject(subscriber);
		return result;
	}

	public boolean isPrimaryProfilePresent(String sim_number) {
		ResultSetWrapper result = findPrimaryProfile(sim_number);

		if(result.getResult() == Result.FAIL && result.getErrorCode()==ErrorCode.SUBSCRIBER_NOT_FOUND) {
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param sim_number
	 * @return
	 */
	public ResultSetWrapper updateSubscriberForResignup(SubscriberInfo subscriber) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside updateSubscriberPassword with subscriber="+subscriber);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);
		try {
			List<SubscriberInfo> subscriberList = subscriberInfoDao.findAllByPrimaryMsisdn(subscriber.getMsisdn());
			String cookeePassword = Util.generateCookeePassword();
			long codeExpiryTime = System.currentTimeMillis() +Util.CODE_EXPIRY_TIME;
			logger.debug("resetting cookeePassword to "+cookeePassword);

			for(SubscriberInfo subs: subscriberList) {
				subs.setCookeepassword(cookeePassword);
				subs.setCode_expiry_time(codeExpiryTime);
				subscriberInfoDao.update(subs);
			}

			// Setting new values to return to user
			subscriber.setCookeepassword(cookeePassword);
			subscriber.setCode_expiry_time(codeExpiryTime);
			result.setObject(subscriber);

			return setDefaultProfile(subscriber.getMsisdn(), subscriber.getPrefix());

		} catch (RecordNotFoundException e) {
			// This should not be the case because subscriber presence was already
			// tested in findPrimaryProfile() api.
			logger.info("Subscriber not found to update password " +e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.SUBSCRIBER_NOT_FOUND);
			return result;
		}

	}

	//public ResultSetWrapper signUp(String preferred_name, String sim_number) {
	public ResultSetWrapper signUp(String sim_number) {
		if(logger.isDebugEnabled()) {
			//logger.debug("Inside signUp with preferred_name="+preferred_name +" and sim_number="+sim_number);
			logger.debug("Inside signUp with sim_number="+sim_number);
		}

		boolean isPrimaryProfilePresent = false;

		ResultSetWrapper result = findPrimaryProfile(sim_number);

		if(result.getResult() == Result.Success) {
			isPrimaryProfilePresent = true;
		}

		// In case primary profile is present just change the password
		if(isPrimaryProfilePresent) {
			result = updateSubscriberForResignup((SubscriberInfo)result.getObject());
			return result;
		}

		//SubscriberInfo subscriberInfo= createSignupProfile(preferred_name, sim_number, 1, sim_number);
		//SubscriberInfo subscriberInfo= createSignupProfile(preferred_name, sim_number, sim_number);
		SubscriberInfo subscriberInfo= createSignupProfile(SignUp.DEFAULT_PROFILE_NAME, sim_number, sim_number);
		result.setObject(subscriberInfo);

		// Create user entry in vsp core service
		vspCoreService.createSignupProfile(sim_number, subscriberInfo.getPrefix(), sim_number, null, true, true);

		// setting default profile
		//setDefaultProfile(sim_number, sim_number);

		// Send SriSms request
		MapJavaThriftHandler.getInstance().submit(new SRISMRequest(sim_number));

		result.setResult(Result.Success);
		return result;
	}

	private int getNextProfilePrefix(String sim_number) {
		ResultSetWrapper result = getAllAssociatedProfile(sim_number);

		if(result.getResult() == Result.FAIL && result.getErrorCode()==ErrorCode.SUBSCRIBER_NOT_FOUND) {
			return 1;
		}

		List<SubscriberInfo> subscriberList = (List<SubscriberInfo>)result.getObject();
		int[] prefixArray = new int[subscriberList.size()];
		int index = 0;

		for(SubscriberInfo subscriber:subscriberList) {
			prefixArray[index] = subscriber.getPrefix();
			index++;
		}
		Arrays.sort(prefixArray);

		return findMissingProfilePrefix(prefixArray, 1, Util.PROFILE_MAX);
	}

	private int findMissingProfilePrefix(int[] a, int first, int last) {
		// before the array: numbers between first and a[0]-1
		for (int i = first; i < a[0]; i++) {
			return i;
		}
		// inside the array: at index i, a number is missing if it is between a[i-1]+1 and a[i]-1
		for (int i = 1; i < a.length; i++) {
			for (int j = 1+a[i-1]; j < a[i]; j++) {
				return j;
			}
		}
		// after the array: numbers between a[a.length-1] and last
		for (int i = 1+a[a.length-1]; i <= last; i++) {
			return i;
		}

		return -1;
	}

	public ResultSetWrapper VerifyCode(String sim_number, String pin) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside VerifyCode with sim_number="+sim_number +" and pin="+pin);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdn(sim_number);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.SUBSCRIBER_NOT_FOUND);
			return result;

		}

		//		int current_retry_count= subscriber.getVerify_retry_max();
		//		subscriber.setVerify_retry_max(++current_retry_count);

		result.setObject(subscriber);

		if(pin.equals(subscriber.getPin())) {
			//updateSubscriberState(subscriber, UserState.CODE_VERIFIRED);
		} else {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.INVALID_VERIFICATION_CODE);
		}

		//		subscriberInfoDao.update(subscriber);
		return result;

	}

	public void updateSubscriberState(SubscriberInfo subscriber, UserState state) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside updateSubscriberState with subscriber="+subscriber +" and state="+state);
		}
		subscriber.setUser_state(state.getState());
		subscriberInfoDao.update(subscriber);

	}

	public void updateSubscriberStateAtConsole(String sim_number, int profile_prefix, UserState userState) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside updateSubscriberStateAtConsole with sim_number="
					+sim_number +" profile_prefix="+profile_prefix +" and userState="+userState);
		}

		Params param = new Params();
		SubscriberDetails subscriberDetails = new SubscriberDetails();
		subscriberDetails.setSim_number(sim_number);
		subscriberDetails.setPhonestate(""+userState.getState());

		param.setSubscriberDetails(subscriberDetails);

		Map<String,Object> postBody = new HashMap<String,Object>();
		postBody.put("method", "g_req_updateSubscriber");
		postBody.put("params",param);

		try {
			String result = HttpURLConnectionManager.sendHttpPost(param, postBody);
		}catch(RuntimeException ex) {
			logger.error("Exception in calling http post ",ex);
		}
	}

	private void updateProfileState(SubscriberInfo subscriber, ProfileState state) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside updateSubscriberState with subscriber="+subscriber +" and state="+state);
		}
		subscriber.setNumb_state(state.getState());
		subscriberInfoDao.update(subscriber);

	}

	public void updateProfileStateAtConsole(String sim_number, int profile_prefix, ProfileState profileState) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside updateProfileStateAtConsole with sim_number="+sim_number +" profile_prefix="+profile_prefix +" and profileState="+profileState);
		}

		Params param = new Params();
		ProfileDetails profileDetails = new ProfileDetails();
		profileDetails.setVsim_number(sim_number);
		profileDetails.setState(""+profileState.getState());

		param.setProfileDetails(profileDetails);

		Map<String,Object> postBody = new HashMap<String,Object>();
		postBody.put("method", "p_req_changeProfileState");
		postBody.put("params",param);

		try {
			String result = HttpURLConnectionManager.sendHttpPost(param, postBody);
		}catch(RuntimeException ex) {
			logger.error("Exception in calling http post ",ex);
		}
	}

	public ResultSetWrapper getPurchaseNumbers() {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getPurchaseNumbers");
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		List<String> purchaseNumList = vspCoreService.getPurchaseNumbers();

		if(purchaseNumList == null || purchaseNumList.size()==0) {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.VIRTUAL_NUMBER_NOT_AVAILABLE);
			return result;
		}

		result.setObject(purchaseNumList);

		return result;
	}

	public void sendValidationSMS(String msisdn, String pin) {
		logger.debug("Inside  sendRegisterSMS with user="+msisdn);
		vspCoreService.sendSMS(msisdn, "Your Temporary pis is "+pin);
	}

	public ResultSetWrapper AuthenticateUser(String sim_number, String password) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside AuthenticateUser with sim_number="+sim_number +" and password="+password);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdn(sim_number);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.SUBSCRIBER_NOT_FOUND);
			return result;

		}

		result.setObject(subscriber);

		logger.debug("subscriber.getCookeepassword()="+subscriber.getCookeepassword());
		logger.debug("password="+password);

		// Temp code 
		if(password.equals(Util.DEFAULT_PASSWORD)) {
			logger.debug("Default password matched");
			//updateSubscriberState(subscriber, UserState.AUTHENTICATED);
		} else if(password.equals(subscriber.getCookeepassword())) {
			//updateSubscriberState(subscriber, UserState.AUTHENTICATED);
		} else {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.INVALID_PASSWORD);
		}

		return result;

	}

	public ResultSetWrapper changeProfileState(String sim_number, ProfileState profileState) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside changeProfileState with sim_number="+sim_number +" and profileState="+profileState);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdn(sim_number);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		updateProfileState(subscriber, profileState);

		return result;	
	}

	public ResultSetWrapper changeProfileState(String sim_number, int profile_prefix, ProfileState profileState) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside changeProfileState with sim_number="+sim_number +" profile_prefix="+profile_prefix +" and profileState="+profileState);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdnAndPrefix(sim_number,profile_prefix);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		updateProfileState(subscriber, profileState);	

		return result;	
	}


	public ResultSetWrapper getDefaultProfile(String sim_number) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getDefaultProfile with sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdn(sim_number);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		result.setObject(subscriber);

		return result;	
	}

	public ResultSetWrapper getAllAssociatedProfile(String sim_number) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getAllAssociatedProfile with sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		List<SubscriberInfo> subscriberList=null;

		try {
			subscriberList = subscriberInfoDao.getAllAssociatedProfiles(sim_number);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.SUBSCRIBER_NOT_FOUND);
			return result;

		}

		result.setObject(subscriberList);

		return result;	
	}
	public ResultSetWrapper getCurrentDefaultProfile(String sim_number) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getCurrentDefaultProfile with sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber = null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdnAndDefaultStatus(sim_number, true);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		result.setObject(subscriber);

		return result;	
	}

	public ResultSetWrapper setDefaultProfile(String sim_number, int profile_prefix) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside setDefaultProfile with sim_number="+sim_number);
		}
		// get current active profile
		ResultSetWrapper resultSet = getCurrentDefaultProfile(sim_number);

		if(resultSet.getResult() == Result.FAIL) {
			return resultSet;
		}
		SubscriberInfo currentActiveSubscriber =  (SubscriberInfo) resultSet.getObject();

		// get profile to be activated
		resultSet = getProfile(sim_number, profile_prefix);

		if(resultSet.getResult() == Result.FAIL) {
			return resultSet;
		}
		SubscriberInfo newlyActiveSubscriber = (SubscriberInfo) resultSet.getObject();

		// resetting default status of current default profile
		changeProfileDefaultStatus(currentActiveSubscriber, false);
		// assigning default status of requested profile
		changeProfileDefaultStatus(newlyActiveSubscriber, true);

		// updating vsp core service table
		vspCoreService.setDefaultNumber(newlyActiveSubscriber.getProfile_number());

		resultSet.setObject(newlyActiveSubscriber);

		return resultSet;	
	}

	public ResultSetWrapper setDefaultProfile(String sim_number, String profile_number) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside setDefaultProfile with sim_number="+sim_number +" and profile_number="+profile_number);
		}

		SubscriberInfo subscriber = (SubscriberInfo) getProfile(sim_number, profile_number).getObject();
		return setDefaultProfile(sim_number, subscriber.getPrefix());
	}

	public void changeProfileDefaultStatus(SubscriberInfo subscriber, boolean defaultStatus) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside changeProfileDefaultStatus with subscriber="+subscriber);
		}

		subscriber.setIs_default(defaultStatus);
		subscriberInfoDao.update(subscriber);
	}

	public ResultSetWrapper getProfile(String sim_number, int profile_prefix) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getProfile with sim_number="+sim_number +" and profile_prefix="+profile_prefix);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdnAndPrefix(sim_number,profile_prefix);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		result.setObject(subscriber);

		return result;	
	}

	public ResultSetWrapper getProfile(String sim_number, String profile_number) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getProfile with sim_number="+sim_number +" and profile_number="+profile_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByProfileNumber(profile_number);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		result.setObject(subscriber);

		return result;	
	}

	//private SubscriberInfo createSignupProfile(String profile_name, String sim_number, int profile_prefix, String profile_number) {
	private SubscriberInfo createSignupProfile(String profile_name, String sim_number, String profile_number) {

		ResultSetWrapper resultSet = createNewProfile(
				profile_number, 
				profile_name, 
				Util.DEFAULT_PROFILE_COLOR,
				sim_number, 
				//profile_prefix,
				Util.generateCookeePassword(),
				Util.DEFAULT_PROFILE_ICON, 
				Util.PROFILE_MAX, 
				Util.VERIFY_RETRY_MAX, 
				Util.generateValidationPin(),
				UserState.AWAIT_CODE.getState(),
				true,
				System.currentTimeMillis() +Util.CODE_EXPIRY_TIME,
				true,
				Util.DEFAULT_FTN,
				Util.DEFAULT_FORWARD_OPTION,
				ProfileState.ON.getState()
				);

		return (SubscriberInfo) resultSet.getObject();
	}

	public ResultSetWrapper createVirtualProfile (
			String profile_number,
			String profile_name,
			String profile_colour,
			String sim_number,
			//int profile_prefix,
			String password,
			int profile_icon,
			int profile_max,
			int verify_retry_max,
			String pin,
			int userState,
			String FTN,
			String forward_option) {

		// This is done as for internal requests, password received is 
		// default password i.e. 123456. whereas for req received from 
		// mobile app it is cookee passowrd. if we create db entry for 
		// this profile with default password, two profile entries for 
		// this user will have different cookee password.
		// 
		ResultSetWrapper resultSet = findPrimaryProfile(sim_number);
		if(resultSet.getResult() == Result.Success) {
			logger.debug("Primary profile present. Resetting cookee password");
			SubscriberInfo subscroberInfo = (SubscriberInfo)resultSet.getObject();
			password = subscroberInfo.getCookeepassword();
		}

		resultSet = new ResultSetWrapper(Result.Success);

		resultSet = createNewProfile(
				profile_number, 
				profile_name, 
				profile_colour,
				sim_number, 
				//profile_prefix,
				password,
				profile_icon, 
				profile_max, 
				verify_retry_max, 
				pin,
				userState,
				false,
				-1,
				false,
				FTN,
				forward_option,
				ProfileState.ON.getState()
				);

		return resultSet;
	}

	public ResultSetWrapper createNewProfile (
			String profile_number,
			String profile_name,
			String profile_colour,
			String sim_number,
			//int profile_prefix,
			String password,
			int profile_icon,
			int profile_max,
			int verify_retry_max,
			String pin,
			int userState,
			boolean is_default,
			long code_expiry_time,
			boolean isPrimary,
			String ftn,
			String forward_option,
			int numbState) {

		if(logger.isDebugEnabled()) {
			logger.debug("Inside createNewProfile with sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		int profile_prefix = getNextProfilePrefix(sim_number);
		if(profile_prefix == -1) {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.MAX_PROFILE_LIMIT_REACHED);
			return result;
		}

		SubscriberInfo subscriberInfo = new SubscriberInfo();
		subscriberInfo.setProfile_number(profile_number);
		subscriberInfo.setProfile_name(profile_name);
		subscriberInfo.setProfile_color(profile_colour);
		subscriberInfo.setMsisdn(sim_number);
		subscriberInfo.setPrefix(profile_prefix);
		subscriberInfo.setCookeepassword(password);
		subscriberInfo.setProfile_icon(profile_icon);
		subscriberInfo.setProfile_max(profile_max);
		subscriberInfo.setVerify_retry_max(verify_retry_max);
		subscriberInfo.setPin(pin);
		subscriberInfo.setUser_state(userState);
		subscriberInfo.setIs_default(is_default);
		subscriberInfo.setCode_expiry_time(code_expiry_time);
		subscriberInfo.setPrimary(isPrimary);
		subscriberInfo.setFTN(ftn);
		subscriberInfo.setForward_option(forward_option);
		subscriberInfo.setNumb_state(numbState);
		subscriberInfoDao.addNew(subscriberInfo);

		result.setObject(subscriberInfo);

		return result;	
	}

	public ResultSetWrapper updateProfile (
			String sim_number,
			String profile_number,
			String password,
			String profile_name,
			String profile_colour,
			int profile_icon,
			int numb_state,
			String FTN,
			String forward_option) {

		if(logger.isDebugEnabled()) {
			logger.debug("Inside updateProfile with sim_number="+sim_number);
		}

		ResultSetWrapper resultSet = getProfile(sim_number, profile_number);

		if(resultSet.getResult() == Result.FAIL) {
			return resultSet;
		}

		SubscriberInfo subscriber = (SubscriberInfo)resultSet.getObject();

		if(profile_name != null) {
			subscriber.setProfile_name(profile_name);
		}
		if(profile_colour != null) {
			subscriber.setProfile_color(profile_colour);
		}
		if(profile_icon != -1) {
			subscriber.setProfile_icon(profile_icon);
		}
		if(numb_state != -1) {
			subscriber.setNumb_state(numb_state);
		}
		if(FTN != null) {
			subscriber.setFTN(FTN);
		}
		if(forward_option != null) {
			subscriber.setForward_option(forward_option);
		}

		subscriberInfoDao.update(subscriber);

		resultSet.setObject(subscriber);

		return resultSet;	
	}

	public ResultSetWrapper isCodeApplicable(String simNumber){

		//ResultSetWrapper result = getAllAssociatedProfile(simNumber);
		//ResultSetWrapper result = getProfile(simNumber, 1);
		ResultSetWrapper result = findPrimaryProfile(simNumber);

		if(result.getResult() == Result.FAIL) {
			return result;
		}

		SubscriberInfo subscriber = (SubscriberInfo)result.getObject();

		//		if(subscriberList.size()>1) {
		//			logger.error("There should not be more than one profile before verification");
		//			result.setResult(Result.FAIL);
		//			result.setErrorCode(ErrorCode.GENERIC_ERROR);
		//			return result;
		//		}
		//
		//		SubscriberInfo subscriber = subscriberList.get(0);
		//
		//		int retry = subscriber.getVerify_retry_max();
		//
		//		if(retry>=Util.VERIFY_RETRY_MAX) {
		//			result.setResult(Result.FAIL);
		//			result.setErrorCode(ErrorCode.VERIFICATION_RETRIES_MAX_REACHED);
		//			return result;
		//
		//		}

		long expirty_time = subscriber.getCode_expiry_time();

		if(System.currentTimeMillis() > expirty_time) {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.CODE_EXPIRED);
			return result;

		}


		return result;		
	}

	/**
	 * check if virtual profile can be created. If request to create virtual profile 
	 * is internal (came from the work-life server), then there will be no sign up 
	 * account for this sim_number.
	 * @param simNumber
	 * @param is_internal
	 * @return
	 */
	public ResultSetWrapper canCreateVirtualProfile(String simNumber, boolean is_internal) {
		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		// temp code
		if(is_internal) {
			return result;
		}

		result = getAllAssociatedProfile(simNumber);

		if(result.getResult() == Result.FAIL) {
			return result;
		}

		List<SubscriberInfo> subscriberList = (List<SubscriberInfo>)result.getObject();

		if(subscriberList.size()>=Util.PROFILE_MAX) {
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.MAX_PROFILE_LIMIT_REACHED);
			return result;
		}

		return result;
	}

	public void purchaseVirtualNumber(String simNumber, String purchaseNumber, int profliePrefix) {
		// for internal requests (req coming from work-life server) virtual number 
		// will be provided by the work-life server and there will be no entry corresponding 
		// to this number in smartclient web service.
		logger.debug("purchasing number");
		vspCoreService.purchaseNumber(purchaseNumber);

		// Send LU request
		logger.debug("Sending LU request");
		vspCoreService.performLocationUpdate(simNumber, purchaseNumber);

		// Creating virtual profile
		logger.debug("Creating virtual profile in vsp core service");
		vspCoreService.createVirtualProfile(simNumber, profliePrefix, purchaseNumber, false, true);
	}

	public void purchaseVirtualNumberInternal(String simNumber, String purchaseNumber, String imsi, int profliePrefix ) {
		// for internal requests (req coming from work-life server) virtual number 
		// will be provided by the work-life server and there will be no entry corresponding 
		// to this number in smartclient web service.

		// Creating virtual profile
		logger.debug("Creating virtual profile in vsp core service");
		vspCoreService.createVirtualProfile(simNumber, profliePrefix, purchaseNumber, imsi, false, true);

		// Send LU request
		logger.debug("Sending LU request");
		vspCoreService.performLocationUpdateWithImsi(purchaseNumber, imsi);

	}

	public ResultSetWrapper deleteProfile(String sim_number, int profile_prefix) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside deleteProfile with sim_number="+sim_number +" and profile_prefix="+profile_prefix);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdnAndPrefix(sim_number,profile_prefix);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;
		}

		// If its primary profile we need to delete all the associated
		// virtual profiles as well.
		if(subscriber.isPrimary()) {
			return deletePrimaryProfile(subscriber);
		}

		// If profile to be deleted is default profile, then we need to make 
		// primary profile as default profile
		if(subscriber.isIs_default()) {
			return deleteDefaultProfile(subscriber);
		}

		subscriberInfoDao.delete(subscriber);
		vspCoreService.deleteProfile(sim_number, profile_prefix);

		return result;	
	}

	public ResultSetWrapper deleteDefaultProfile(SubscriberInfo subscriber) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside deleteDefaultProfile with subscriber="+subscriber);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		try {
			SubscriberInfo primaryProfile = subscriberInfoDao.findPrimaryProfile(subscriber.getMsisdn());
			primaryProfile.setIs_default(true);
			subscriberInfoDao.update(primaryProfile);

		} catch (RecordNotFoundException e) {
			logger.info("Primary profile not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;
		}

		subscriberInfoDao.delete(subscriber);
		vspCoreService.deleteProfile(subscriber.getMsisdn(), subscriber.getPrefix());

		return result;	
	}

	public ResultSetWrapper deletePrimaryProfile(SubscriberInfo subscriber) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside deletePrimaryProfile with subscriber="+subscriber);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		List<SubscriberInfo> subscriberList;
		try {
			subscriberList = subscriberInfoDao.getAllAssociatedProfiles(subscriber.getMsisdn());
		} catch (RecordNotFoundException e) {
			logger.info("Unable to find all associated profiles ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;
		}

		for(SubscriberInfo subs :subscriberList) {
			subscriberInfoDao.delete(subs);
			vspCoreService.deleteProfile(subs.getMsisdn(), subs.getPrefix());

		}

		return result;	
	}

	public ResultSetWrapper getUserState(String sim_number) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside getUserState with sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdnAndPrefix(sim_number,0);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		result.setObject(subscriber);
		return result;	

	}

	public ResultSetWrapper setCallForwardingNumber(String sim_number, 
			int profile_prefix, 
			String FTN) {
		if(logger.isDebugEnabled()) {
			logger.debug("Inside setCallForwardingNumber with sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);

		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdnAndPrefix(sim_number,profile_prefix);
		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		subscriber.setFTN(FTN);
		subscriberInfoDao.update(subscriber);

		result.setObject(subscriber);
		return result;	
	}

	public ResultSetWrapper setCallForwardingState(String sim_number, 
			int profile_prefix, 
			String forward_option) {

		if(logger.isDebugEnabled()) {
			logger.debug("Inside setCallForwardingState with sim_number="+sim_number);
		}

		ResultSetWrapper result = new ResultSetWrapper(Result.Success);
		SubscriberInfo subscriber=null;

		try {
			subscriber = subscriberInfoDao.findByPrimaryMsisdnAndPrefix(sim_number, profile_prefix);

		} catch (RecordNotFoundException e) {
			logger.info("Subscriber not found ",e);
			result.setResult(Result.FAIL);
			result.setErrorCode(ErrorCode.GENERIC_ERROR);
			return result;

		}

		subscriber.setForward_option(forward_option);
		subscriberInfoDao.update(subscriber);

		result.setObject(subscriber);
		return result;	
	}

	public ResultSetWrapper getCallForwardingStatus(String sim_number, 
			int profile_prefix) {

		if(logger.isDebugEnabled()) {
			logger.debug("Inside getCallForwardingStatus with sim_number="+sim_number);
		}
		return getProfile(sim_number, profile_prefix);
	}

}
